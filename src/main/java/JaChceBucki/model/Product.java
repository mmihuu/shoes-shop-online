package JaChceBucki.model;




import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity

public class Product implements Serializable {



    private static final long serialVersionUID = 10L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long productId;
    @NotEmpty
    private String productName;
    @NotEmpty
    private String productBrand;
    @NotEmpty
    private String productModel;
    @NotEmpty
    private String productNumber;

    @Range(min=0)
    @NotNull
    private BigDecimal productPrice;

    @Range(min=0)
    @NotNull
    private Integer unitInStock;
    private String productCategory;
    @NotEmpty
    private String productDescription;

    private String productStatus;


    @Range(min=0)
    @NotNull
    private BigDecimal discount;

    @Transient
    private MultipartFile productImage;

    @Transient
    private Map<String,String> categoryList;

    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonIgnore
    private List<CartItem> cartItems;



    public Map<String, String> getCategoryList(){


        return categoryList;
    }


    public List<CartItem> getCartItems(){
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems){

        this.cartItems = cartItems;
    }


    public Product(String productName, String productBrand, String productModel, BigDecimal productPrice, int unitInStock,
                   String productCategory, String productDescription, String productStatus, BigDecimal discount,
                   MultipartFile productImage) {
        super();
        this.productName = productName;
        this.productBrand = productBrand;
        this.productModel = productModel;
        this.productPrice = productPrice;
        this.unitInStock = unitInStock;
        this.productCategory = productCategory;
        this.productDescription = productDescription;
        this.productStatus = productStatus;
        this.discount = discount;
        this.productImage = productImage;
    }

    public Product() {


        categoryList=new HashMap<>();
        categoryList.put("Tenisówki", "Tenisówki");
        categoryList.put("Adidasy", "Adidasy");
        categoryList.put("Kozaki", "Kozaki");
        categoryList.put("Półbuty", "Półbuty");
        categoryList.put("Sandały", "Sandały");

    }





}
