package JaChceBucki.model;



import lombok.Data;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;



@Entity
public @Data
class BillingAddress implements Serializable {

    private static  final long serialVersionUID = 10L;




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long billingAddressID;


    @NotEmpty
    private String zipCode;

    @NotEmpty
    private String country;

    @NotEmpty
    private String voivodeship;

    @NotEmpty
    private String street;

    @NotEmpty
    private String houseNumber;















}
