package JaChceBucki.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Entity
public class DeliveryAddress implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long shippingAddressID;


    @NotEmpty
    private String zipCode;

    @NotEmpty
    private String country;

    @NotEmpty
    private String voivodeship;

    @NotEmpty
    private String street;

    @NotEmpty
    private String houseNumber;





}
