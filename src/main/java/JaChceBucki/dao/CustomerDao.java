package JaChceBucki.dao;

import JaChceBucki.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerDao extends CrudRepository<Customer, Long> {

}
