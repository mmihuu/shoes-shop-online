package JaChceBucki.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryAddressDao extends CrudRepository<DeliveryAddressDao,Long> {

}
