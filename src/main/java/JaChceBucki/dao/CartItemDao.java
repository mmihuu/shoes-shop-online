package JaChceBucki.dao;

import JaChceBucki.model.CartItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemDao extends CrudRepository<CartItem, Long> {
}
