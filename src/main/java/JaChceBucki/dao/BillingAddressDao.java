package JaChceBucki.dao;

import JaChceBucki.model.BillingAddress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingAddressDao extends CrudRepository<BillingAddress, Long> {


}
