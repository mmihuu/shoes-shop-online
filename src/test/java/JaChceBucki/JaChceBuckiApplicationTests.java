package JaChceBucki;

import JaChceBucki.model.Product;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@SpringBootTest
class JaChceBuckiApplicationTests {

	static Validator validator;


	@BeforeClass
	public static void setupValidatorInstance() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}



	@Test
	void contextLoads() {
	}




	@Test
	public void Product_notEmptyAnnotation(){

		Product product = new Product();

		Set<ConstraintViolation<Product>> violations = validator.validate(product);



	}

}
